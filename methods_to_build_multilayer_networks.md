# Supplemental methods to build the multilayer networks

---

Michaël Pierrelée, Aix Marseille Univ, CNRS, IBDM, UMR7288, FRANCE - <michael.pierrelee@univ-amu.fr>

*Apache License 2.0*

---

This file describes the steps to reproduce the data analysis of the paper.

We presented two datasets: the **yeast dataset** (re-analysis of a RNA-seq dataset about the cell-cycle) and the **mouse dataset** (original analysis of a RNA-seq dataset about mouse inflammation). In this document, they are referred to one or the other by simply “yeast“ or “mouse“. Each was processed by independent pipelines.

More information can be found at the beginning of each notebook, where the header describes precisely the steps of the workflow.

## Setup working environment

Data analysis was performed with Python and R within conda environments.

### Install conda

* Download latest conda version https://conda.io/miniconda.html.
* Start conda installation `bash Miniconda3-latest-Linux-x86_64.sh`.
* Configure conda:

```bash
conda config --add channels defaults
conda config --add channels bioconda
conda config --add channels conda-forge
conda config --set auto_activate_base False
```

### Setup fastq processing environment

Conda environment to process the fastq files and get the count matrix. **Python** (v3.8.5) and **R** (v3.6.0) were used.

**Option 1:** the followed procedure was applied to install the environment.

```bash
conda create -n timenexus_fastq_env
conda activate timenexus_fastq_env
conda install fastqc multiqc star cutadapt sra-tools subread
```

**Option 2:** to get the exact same environment as we used, one can import the environment file into conda.

```bash
conda env create -f environments/timenexus_fastq_env.yml
conda activate timenexus_fastq_env
```

### Setup Python/R environment

Conda environment to build the input data for the paper and analyze the results.

**Option 1:** initial procedure.

```bash
conda create -n timenexus_env
conda activate timenexus_env
conda install python scipy numpy pandas matplotlib seaborn networkx scikit-learn ipykernel r bioconductor-edger r-irkernel r-statmod gseapy openpyxl
```

**Option 2:** import environment.

```bash
conda env create -f environments/timenexus_env.yml
conda activate timenexus_env
```

**Note:** in practice, we installed two environments: `timenexus_env`, whose the installation procedure is described below, and another to run the Jupyter notebooks. The packages `ipykernel` and `r-irkernel` were setup to discover the Python and R kernels from `timenexus_env` by the Jupyter environment, but these two packages are *per se* not mandatory to run the scripts. To setup a dedicated Jupyter environment:

```bash
conda create -n jupyter_env
conda activate jupyter_env
conda install python jupyterlab nb_conda_kernels nodejs
```

Then, start `jupyterlab` from `jupyter_env`. It will discover the Python and R kernels from  `timenexus_env`, so you can run the scripts below.

## Process experimental datasets

### Data pre-processing for the yeast dataset

The IDs of yeast transcripts were their systematic/locus names (ex: YAL065C) to avoid any ambiguity. 

#### Fetch data

We used the yeast dataset with the GEO number [GSE80474](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE80474) of [Kelliher, et al. (2016) https://doi.org/10.1371/journal.pgen.1006453](https://doi.org/10.1371/journal.pgen.1006453). 

1.  We downloaded the 36/50 samples for S. cerevisiae culture, wild-type, from 0 to 175 min, each 5 min, by getting the [accession list from the SRA Run Selector](https://www.ncbi.nlm.nih.gov/Traces/study/?acc=GSE80474).
   * Accession list: `data-create_networks/yeast_Kelliher_2016/AccessionList_PRJNA319029_yeast_WT.txt`.
2. The files were fetched and converted to fastq using `fasterq-dump` of **[SRA toolkit](https://www.ncbi.nlm.nih.gov/books/NBK158900/)** (v2.10.8).
   * Run SRA toolkit: `scripts-create_networks/1-process_fastq/1-fetch_sra_files.sh`.

#### Get the gene counts

1. Create the **STAR** (v2.7.5a) index of the yeast genome.
   1. Download the *S. cerevisiae* S288C genome (Ensembl-build R64-1-1) from [Illumina iGenomes](https://support.illumina.com/sequencing/sequencing_software/igenome.html). Within the downloaded folder, the files of interest are the GTF file  `Annotation/Genes/genes.gtf` and the whole genome fasta file `Sequence/WholeGenomeFasta/genome.fa`.
   2. Apply STAR . `--sjdbOverhang` was set to 49 because the samples are 50 base pairs long and `--genomeSAindexNbases` was set to 10 according to [this conversation](https://groups.google.com/forum/#!topic/rna-star/hQeHTBbkc0c).
      * Run STAR: `scripts-create_networks/1-process_fastq/2-generate_STARIndex.sh`. Note: not all fastq files could be fetched at the first try, so a second run was performed for the failed ones. 
2. Fastq files were controlled with **FastQC** (v0.11.9) and then mapped by **STAR**: `scripts-create_networks/1-process_fastq/3-STAR_mapping.sh` (use a loop).
3. Counts were generated by **featureCounts** (v2.0.1): `scripts-create_networks/1-process_fastq/4-featurecounts.sh`.
   * File: `data-create_networks/yeast_Kelliher_2016/yeast_WT_counts.txt`.
4. **MultiQC** (v1.9) report was returned from FastQC, STAR and featureCounts files: `scripts-create_networks/1-process_fastq/5-submit_multiqc.sh`.
   * File: `data-create_networks/yeast_Kelliher_2016/multiqc.html`.

Script logs: `data-create_networks/yeast_Kelliher_2016/sh_outputs/`.

### Data processing for the mouse dataset

The Moqrich Team sampled neurons of dorsal root ganglions from C57BL/6J wild-type strains. The samples were sequenced by GATC Biotech company.

We processed the raw fastq files as above:

1. STAR index of the *M. musculus* genome (UCSC-build mm10) from [Illumina iGenomes](https://support.illumina.com/sequencing/sequencing_software/igenome.html): `scripts-create_networks/1-process_fastq/mouse/2-generate_STARIndex.sh` (without the parameters above).
2. Quality control with FastQC: `scripts-create_networks/1-process_fastq/mouse/2-fastqc_raw.sh`.
3. As the control showed primer contaminations, perform adapter trimming with **Cutadapt** (v2.10), then another quality control and mapping: `scripts-create_networks/1-process_fastq/mouse/3-cutadapt-fastqc_trimmed-star.sh`.
4. Read alignment with STAR (default parameters): `scripts-create_networks/1-process_fastq/mouse/3-cutadapt-fastqc_trimmed-star.sh`.
5. Gene counting: `scripts-create_networks/1-process_fastq/mouse/4-featurecounts.sh`.

   * `data-create_networks/mouse/mouse_DRG_WT_timecourse_counts.txt`.
6. MultiQC report:  `scripts-create_networks/1-process_fastq/mouse/5-multiqc.sh`.

Script logs: `data-create_networks/mouse/sh_outputs/`.

### Get normalized counts for the yeast dataset

Counts were normalized with the TMM normalization from **EdgeR** (v3.28.0). It returned logCPM (log counts per million). Only the samples from 25 min to 100 min (1st cell cycle) were used.

Jupyter Notebooks:

1. `scripts-create_networks/2-differential_expression_analysis/1-quality_raw_counts.PY.ipynb`: quality control of raw counts.

2. `scripts-create_networks/2-noRep-TMM_normalization.R.ipynb`: normalize raw counts.

Normalized counts: `data-create_networks/yeast_Kelliher_2016/yeast_WT_logCPM_noRep.txt`.

### Get differentially expressed genes for the mouse dataset

Differentially expressed analysis was performed by comparing each of the 3 time points (days 1, 3 and 30) to the baseline (day 0), as for the yeast dataset.

Jupyter Notebooks:

1. `scripts-create_networks/2-differential_expression_analysis/mouse/1-mouse_quality_raw_counts.PY.ipynb`: quality control of raw counts.
2. `scripts-create_networks/2-differential_expression_analysis/mouse/2-mouse_differential_expression_analysis.R.ipynb`: perform the differential expression analysis with EdgeR.
   * Results within the folder: `data-create_networks/mouse/comparisons/`.
3. `scripts-create_networks/2-differential_expression_analysis/mouse/3-mouse_DEA_quality.PY.ipynb`: quality control of the differential expression analysis.

## Build multi-layer networks

### Get files

* **Protein-protein interactions:** [HitPredict](http://www.hitpredict.org/), for *S. cerevisiae* and *M. musculus*, 2020-08-03 release (the access was provided by the authors). We used the columns with the protein IDs from UniProt (accession numbers) for the yeast dataset and those with the gene names for the mouse dataset. Files were not added to the GitLab repository as it was prohibited by the authors:
  * `data-create_networks/hitpredit-03Aug2020/S_cerevisiae_interactions_MITAB-2.5.txt`.
  * `data-create_networks/hitpredit-03Aug2020/M_musculus_interactions_MITAB-2.5.txt`.
* **Cross-references:** UniProt <> SGD for yeast from [UniProt](https://www.uniprot.org/docs/yeast), 2020-06-17 release.
  * File: `data-create_networks/cross_ref-UniProt_SGD-17Jun2020/yeast.txt`.
* **Transcription factors:** regulation interactions were provided in August 2020 by Yeastract’s authors as a bulk flat file.
  * File (not provided as sharing was prohibited by the authors): `data-create_networks/yeastract_TF/yeastract2019-flat-file.tsv`.
* **KEGG cell-cycle proteins:** proteins involved in the pathway responsible of the yeast cell-cycle. They were retrieved from the [KEGG pathway](http://rest.kegg.jp/get/pathway:sce04111) `sce04111 Cell cycle - yeast` (v20AUG2020).
  * File: `data-create_networks/cell_cycle_genes/20AUG2020_KEGG_sce04111_genes.txt`.

### Generate the yeast multilayer-network tables for TimeNexus

1. `scripts-create_networks/3-prepare_interactome/1-yeastract_network.PY.ipynb`: prepare protein-DNA interactions (PDIs). Duplicated entries and self-loops were filtered out.
   * Cleaned file: `data-create_networks/yeastract_TF/protein_DNA_interactions.tsv`.
2. `scripts-create_networks/3-prepare_interactome/2-hitpredict_network.PY.ipynb`: prepare protein-protein interactions (PPIs). Protein IDs were converted into locus names and self-loops as well as interactions with a score lower to 0.281 were filtered out. The score cutoff was set as recommended by Lòpez et al. (2015).
   * Cleaned file: `data-create_networks/hitpredit-03Aug2020/protein_protein_interactions.tsv`.
3. `scripts-create_networks/3-prepare_interactome/3-noRep-multilayer_network_tables.PY.ipynb`: prepare tables of the multi-layer network from the PDIs and PPIs.
   1. The multilayer network was limited to 16 layers going from 25 min to 100 min (1 layer every 5 min), which corresponded to the first cell-cycle in the Kelliher’s dataset.
   2. We generated an undirected network from PPIs and PDIs. Before the aggregated, multi-PPIs between node pairs were aggregated by taking the means of the confidence scores. The scores of aggregated edges from PPIs and PDIs were computed with `(2 * P + D) / 4`, where P is the PPI score and D is the number of PDI edges between the node pair. Indeed, we assumed that an undirected PPI is equivalent to 2 edges and that PDIs have a confidence score of 1.
   3. Node weights were computed with: `|logCPM - avgLogCPM|`. `avgLogCPM` is the mean of logCPM across time for each gene. Nodes which were not present in the RNA-sequencing dataset were filtered out.
   4. A node was tagged as a query for a given layer if the absolute value of the log fold change is higher than 0.5 at this layer.
   6. Inter-layer edge's weights were computed using `( wDys1 + wDys2 ) / ( 1 + wDys1 + wDys2 )`, where wDys1 and wDys2 are the weights of the node in the layer i and i+1, respectively.
   7. Tables for the multi-layer network: `data-create_networks/yeast_multiLayerNetwork/`.

A column with the gene names can be added to the node table of the flattened network by using the file `data-create_networks/cell_cycle_genes/convertKeggGeneNamesOfFlattenedNetwork.txt`, or to the aggregated network with the file `data-create_networks/cell_cycle_genes/20AUG2020_KEGG_sce04111_genes.txt`.

### Generate the mouse multilayer-network tables for TimeNexus

1. `scripts-create_networks/3-prepare_interactome/mouse/1-mouse_hitpredict_network.PY.ipynb`: prepare protein-protein interactions (PPIs). Self-loops as well as interactions with a score lower to 0.281 were filtered out.
   * Cleaned file: `data-create_networks/hitpredit-03Aug2020/mouse_ppi.tsv`.
2. `scripts-create_networks/3-prepare_interactome/mouse/2-mouse_multi-layer_network_tables.PY.ipynb`: prepare tables of the multi-layer network from the PPIs.
   1. The process was the same as above, but for the 3 layers (day 1 vs. 0, 3 vs. 0 and 30 vs. 0) and there was no PDI to aggregate.
   2. Tables for the multi-layer network: `data-create_networks/mouse_multiLayerNetwork/`.

### Generate networks to test the robustness of TimeNexus

These files can be found in the folder: `data-create_networks/yeast_multiLayerNetwork/robustness/`.

We generated 3 types of multilayer networks from the yeast multilayer network:

* **`Perfect` intra-layer edge weights**: a weight of 1 was set to each intra-layer edge.
  * `perfectEdgeWeights_intraLayerEdgeTable_noRep.tsv`
* **`Permuted` nodes**: the node names of the node table were shuffled, so the biological meaning of the network was changed but not its topology.
  * `permutedNodes_nodeTable_noRep.tsv`
* **`Random` intra-layer edge weights**: a random weight following the uniform distribution [0.01,1) was set to each intra-layer edge.
  * `randomEdgeWeights_intraLayerEdgeTable_noRep.tsv`

## Extract subnetworks

### For the yeast dataset

The subnetworks were extracted by **TimeNexus** on **Cytoscape** (v3.8.0), with either the **PathLinker** app (v1.4.2) or the Anat server.

The connexion to the server was retro-engineered from the **AnatApp** (v2.0.0); it involves that an Internet connexion is needed to use TimeNexus with Anat, but the app itself does not require to be installed. On the contrary, TimeNexus connects to PathLinker through its CyRest interface, so it can be used in local.

We followed this protocol to generate subnetworks for the yeast multilayer network:

1. Load into Cytoscape the tables of the yeast multilayer network: *Toolbar > Import table from file*. For each table, select it, give it a name and import it as “unassigned table” with the default Cytoscape parameters.
   1. The files to load: `data-create_networks/yeast_multiLayerNetwork/`.
2. Open TimeNexus Converter: *Menu > Apps > TimeNexus > Convert networks or tables into MLN*.
3. Define how to load the multilayer network:
   1. Set the parameters: **number of layers = 16**, default weights = 1 (default), node-aligned network = true (default), edge-aligned network = true (default), **automatic inter-layer coupling = false, inter-layer coupling is equivalent = true**, intra-layer edges are directed = false (default), inter-layer edges are directed = true (default), all nodes are query nodes = false.
   2. For the node table, select the corresponding table and set the column types: column “**Node**” = type “**Node**”, “**Core**” = “**Shared column**”, columns “**Weight**” 1 to 16 = types “**Node weight layer\_**” 1 to 16, columns “**Query**” 1 to 16 = “**Other columns layer\_**”.
   3. For the intra-layer edge table, select the corresponding table and set the column types: column “**source**” = type “**Source node**”, “**target**” = “**Target node”**, “**score**” = “**Edge weight**”, “**Edge type**” = “**Shared column**”.
   4. For the inter-layer edge table, select the corresponding table and set the column types: column “**source**” = type “**Source node**”, “**target**” = “**Target node”**, columns “**Weight**” 1 > 2 to 15 > 16  = types “**Edge-weight layers\_**” 1 > 2 to 15 > 16.
   5. Click on “convert”. It will generate a multilayer network within Cytoscape. For convenience, we saved the session and loaded it again when needed.
4. Run an extraction with TimeNexus: *Left-side bar > TimeNexus Extractor*.
   1. Click on “Load multi-layer networks”. If the above multilayer network is alone, it will be select as well as its 16 layers by default.
   2. **Verify the multilayer network = false** (check the box for the first run with this multilayer network).
   3. **Select the extraction method (ex: Pairwise).**
   4. **Select the extracting app** and set its parameters:
      1. **For PathLinker: k = 750** (for example), Edge penalty = 1 (default), Edge weights = PROBABILITIES (default), Network is directed = false (default), Allow sources and targets in path = true (default), Include tied paths = false (default), cyRest port = 1234 (default if not changed by the user in the Cytoscape parameters).
      2. **For Anat: algorithm type = Anchored, sub-algorithm = Approximate**, global-local balance = 0.25 (default), Unweighted network = false (default), margin = 0 (default), edge penalty = 25 (default), enable node penalty = false (default), predict anchors = false (default), run to completion = false (default).
   5. **Select columns with query nodes:**
      1. **For Pairwise and One by One:** Layer 1 to 16 = Query 1 to 16.
      2. **For Global:** Layer 1 = Query 1 and Layer 16 = Query 16. The other layers do not matter.
   6. Click on “Extract subnetworks” and wait until completion or an error of the app. If the extraction is successful, it generates a new multilayer network called “Extracted network”.
5. To export a multilayer network: *right-click on its flattened network > Export as network > Export File Format = GraphML.*
   * The extracted flattened networks were exported at `data-analyze_results/pathlinker_yeast/` for PathLinker,
   * and `data-analyze_results/anat_yeast/` for Anat.
6. To load back a multilayer network: *Toolbar > Import network From File > Select a flattened network which was exported*. Then, use the tool *Menu > Apps > TimeNexus > Build MLN from flattened network*.
7. Make a view of the multilayer network with TimeNexus: *Left-side bar > TimeNexus Viewer*.
   1. Click on “Load multi-layer networks”. If the above multilayer network is alone, it will be selected by default.
   2. If one wants to show the layers 3 to 6 for example: **select the layer to focus = 3, number of adjacent layers = 3**, show adjacent layers = forward (default).
   3. Update the view.

For more details, see the [documentation](https://gitlab.com/habermann_lab/timenexus-cytoscape-app/-/blob/master/documentation/doc.md).

### For the mouse dataset

The same principle was applied to the mouse dataset, with only **PathLinker** K=50 and 3 layers (instead of 16).
